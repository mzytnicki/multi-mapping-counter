/*
mmquant, a multi-mapping quantification tool.
Copyright (C) 2016-2017 Matthias Zytnicki

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mmquant.h"

int main(int argc, char **argv) {
	std::vector < std::string > args (argv, argv + argc);
	int code;
	MmquantParameters parameters;
	parameters.geneOverlapFunction = geneInclusion;
	code = parameters.parse(args);
	if (code == EXIT_FAILURE) return EXIT_FAILURE;
	if (code == -1) return EXIT_SUCCESS;
	return start(parameters);
}
