# README #


### Why using this tool? ###

This tool counts the number of reads (produced by RNA-Seq) per gene, much like [HTSeq-count](http://www-huber.embl.de/users/anders/HTSeq/doc/overview.html) and [featureCounts](http://bioinf.wehi.edu.au/featureCounts/).  The main difference with other tools is that multi-mapping reads are counted differently: if a read is mapped to gene A, gene B, and gene C, the tool will create a new feature, "geneA--geneB--geneC", that will be counted once.

##### Why it matters? #####

Recently, an [article](http://www.genomebiology.com/2015/16/1/177) showed that RNA-Seq quantification tools are not accurate, leading to errors while finding differentially expressed genes.  The authors suggest this method, that may not provide the genes that are differentially expressed (something that RNA-Seq alone cannot do), but the groups of genes that are differentially expressed.

### How to use it? ###

Here are the options:

Compulsory options:

* `-a` *file*: annotation file in GTF format
* `-r` *file1* [*file2*] ...: reads in BAM/SAM format

Main options:

* `-o` *output*: output file (see *infra*). Default: standard output.
* `-n` *name1* [*name2*] ...: short name for each of the reads files
* `-s` *strand*: can be:
    * for paired-end reads: `U` (unknown), `FR` (forward-reverse), `RF` (reverse-forward), `FF` (forward-forward);
    * for single-end reads: `U` (unknown), `F` (forward), `R` (reverse);
    * Default: `U`.
    * (you can provide different strand types if the sequencing strategies differ in your samples)
* `-e` *sorted*: can be `Y` if the reads are sorted (default), and `N` otherwise. (You can type `-t Y N N` if only the first -among three- input file is sorted, or `-t N` if none is sorted.)
* `-f` format (`SAM` or `BAM`): format of the read input files
* `-l` *number* overlap type (see *infra*). Default: -1.

Ambiguous reads options:

* `-m` *float*: merge threshold (see *infra*)
* `-c` *integer*: count threshold (see *infra*)
* `-d` *integer*: number of overlapping bp between the best matches and the other matches (see *infra*). Default: 30
* `-D` *float*: ratio of overlapping bp between the best matches and the other matches (see *infra*). Default: 0.5

Output options:

* `-g`: use gene name instead of gene ID in the output file (see *infra*)
* `-O` file_name: print statistics to a file instead of stderr
* `-F`: use featureCounts output style
* `-G` *string*: provide the first column name in the output file (default: `Gene`)
* `-p`: print progress
* `-t` *integer*: number of threads (one thread per SAM/BAM file)
* `-h`: help

##### Annotation file #####

The annotation file should be in GTF.  GFF might work too.  The tool only uses the gene/transcript/exon types.

##### Reads files #####

The reads should be given in SAM or BAM format, and be sorted (by position).  The reads can be single end or paired-end (or a mixture thereof).

You can use the [samtools](http://www.htslib.org/) to sort them.  This tool uses the NH flag (provides the number of hits for each read, see the [SAM format specification](https://samtools.github.io/hts-specs/SAMv1.pdf)), so be sure that your mapping tool sets it adequately (yes, [TopHat2](http://ccb.jhu.edu/software/tophat/index.shtml) and [STAR](https://github.com/alexdobin/STAR/releases) do it fine).  You should also check how your mapping tool handles multi-mapping reads (this can usually be tuned using the appropriate parameters).

##### Output file #####

The output is a tab-separated file, to be use in EdgeR or DESeq, for instance.  If the user provided *n* reads files, the output will contain *n+1* columns:

Gene           | sample_1 | sample_2 | ...
---------------|----------|----------|----
gene_A         | ...      | ...      | ...
gene_B         | ...      | ...      | ...
gene_B--gene_C | ...      | ...      | ...

The first line is the ID of the genes.
If a read maps several genes (say, gene_B and gene_C), a new feature is added to the table, gene_B--gene_C.  The reads that can be mapped to these genes will be counted there (but not in the gene_B nor gene_C lines).

With the `-g` option, the gene names are used instead of gene IDs.  If two different genes have the same name, the systematic name is added, like: Mat2a (ENSMUSG00000053907).

Note that the gene IDs and gene names should be given in the GTF file after the `gene_id` and `gene_name` tags respectively.

##### Read mapping to several genes #####

We will suppose here that the `-l 1` strategy is used (i.e. a read is attributed to a gene as soon as at least 1 nucleotide overlap).  The example can be extended to other strategies as well.

If a read (say, of size 100), maps unambiguously and overlaps with gene A and B, it will be counted as 1 for the new "gene" gene_A--gene_B.  However, suppose that only 1 nucleotide overlaps with gene A, whereas 100 nucleotides overlap with gene B (yes, genes A and B overlap).  You probably would like to attribute the read to gene B.

The options `-d` and `-D` control this.  We compute the number of overlapping nucleotides between a read and the overlapping genes.  If a read overlaps "significantly" more with one gene than with all the other genes, they will attribute the read to the former gene only.

The option `-d` *n* computes the differences of overlapping nucleotides.  Let us name *N_A* and *N_B* the number of overlapping nucleotides with genes A and B respectively.  If *N_A >= N_B + n*, then the read will be attributed to gene A only.

The option `-D` *m* compares the ratio of overlapping nucleotides. If *N_A / N_B >= m*, then the read will be attributed to gene A only.

If both option `-d` *n* and `-D` *m* are used, then the read will be attributed to gene A only iff both *N_A >= N_B + n* and *N_A / N_B >= m*.

##### Output stats #####

The output stats are given in standard error.

The general shape is

    Results for sample_A:
        # hits:                     N
        # uniquely mapped reads:    N (x%)
        # ambiguous hits:           N (x%)
        # non-uniquely mapped hits: N (x%)
        # unassigned hits:          N (x%)

These figures mainly provide stats on hits; one sequence may have zero, one, or several hits.  An ambiguous hit is a hit that overlaps several annotation features.  A non-uniquely mapped hit belongs to a sequence that maps several loci in the genome.

##### Overlap #####

The way a read R is mapped to a gene A depends on the `-l` *n* value:

if *n* is            | then R is mapped to A iff
---------------------|-----------------------------------------------
a negative value     | R is included in A
a positive integer   | they have at least *n* nucleotides in common
a float value (0, 1) | *n*% of the nucleotides of R are shared with A

##### Merge Threshold #####

Sometimes, there are very few reads that can be mapped unambiguously to a gene A, because it is very similar to gene B.

Gene           | sample_1 
---------------|----------
gene_A         | *x*      
gene_B         | *y*      
gene_A--gene_B | *z*      

In the previous example, suppose that *x<<z*.  In this case, you can move all the reads from gene_A to gene_A--gene_B, using the merge threshold *t*, a float in (0, 1).  If *x < t y*, then the reads are transferred.

##### Count Threshold #####

If the maximum number of reads for a gene is less than the count threshold (a non-negative integer), then the corresponding line is discarded.


##### Using several input files #####

If all the input files have the BAM format, and are, say forward-reverse, you can type `mmquant -a ann.gtf -r file1.bam file2.bam -s FR`.  If the format and/or the library strategies differ, you can declare it this way: `mmquant -a ann.gtf -r file1.dat file2.dat -s FR RF -f SAM BAM`


### How to set up? ###

* For the moment, the tool is only two C++ files: `mmquant.cpp` and `mmquant.h`.
* Compile it with `make`.
* You will need a C++11 compiler, and zlib.
* If this does not work, a static build, `mmquant_static` is also available.


### Troubleshooting ###

#### My compiler says `cc1plus: error: unrecognized command line option '-std=c++11'` ####

The code is written in C++11, like many other bioinformatics tools.  C++11 offers many new functions to the developper, and it seems that your compiler is too old for C++11.  Starting from 4.7, released in 2012, GCC supports the C++11 features present in mmquant.  You could consider upgrading to this version at least.



### Contact ###

Comment? Suggestion? Do not hesitate sending me an [email](mailto:matthias.zytnicki@toulouse.inra.fr)
