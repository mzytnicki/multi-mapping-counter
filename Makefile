CXX = g++
CFLAGS += -Wall -std=c++11 -O3
LDFLAGS += -pthread -lz
mmquant: mmquant.cpp
	$(CXX) mmquant.cpp -DMMSTANDALONE $(CFLAGS) -o mmquant $(LDFLAGS)
clean:
	\rm mmquant
