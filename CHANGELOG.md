# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## 2017-09-20
### Changed
- Corrected cosmetic errors so that it compiles on a Mac (thanks to CBM Oudejans).
- Corrected "-t" parameter conflict (thanks to CBM Oudejans).

## 2017-10-19
### Added
- featureCounts-style output with "-O" option.

## 2018-01-06
### Changed
- Corrected cosmetic error so that it compiles on a Mac (thanks to Anna Feitzinger).

## 2018-03-20
### Added
- Stats output redirection with "-O" option.
### Changed
- featureCounts-style output to "-F".
- Major rewrite to make it compatible with R(cpp).

## 2023-03-30
### Changed
- Improve GFF3 parser.

## 2023-10-17
### Added
- Parameter for the first column name of the output file.
